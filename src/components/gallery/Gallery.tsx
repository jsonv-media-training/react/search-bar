import React from "react";
import { UnslashImages } from "../models/Unslash";
import ImageCard from "../image-cards/image-card";
import "./Gallery.scss";

class Gallery extends React.Component<UnslashImages, any> {

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <div className="gallery mt-4">
        {
          this.props.images.map(img => {
            return (
              <ImageCard key={img.id} image={img} />
            )})
        }
      </div>
    );
  }
}

export default Gallery;
