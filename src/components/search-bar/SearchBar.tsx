import React, { SyntheticEvent, FormEvent } from "react";
import "./SearchBar.scss";

interface SearchTerm {
  key: string
}

class SearchBar extends React.Component<{ invokeFnc (key: string): void }, SearchTerm> {

  state: SearchTerm = { key: '' };

  private onInputChange = (event: SyntheticEvent) => {
    const key = (event.target as HTMLInputElement).value;
    this.setState({ key });
  };

  private onSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    this.props.invokeFnc(this.state.key);
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <form className="search-wrapper active" onSubmit={this.onSubmit}>
        <button type="button" className="search-btn btn">
          <i className="fas fa-search"/>
        </button>
        <input className="search-input" type="text" name="search" placeholder="Enter something ..."
               value={this.state.key}
               onChange={this.onInputChange}/>
      </form>
    );
  }
}

export default SearchBar;
