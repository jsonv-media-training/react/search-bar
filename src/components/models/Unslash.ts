export interface UnslashImage {
  id: string,
  description: string,
  alt_description: string,
  height: number,
  width: number,
  created_at: Date,
  urls: {
    full: string,
    raw: string,
    regular: string,
    small: string,
    thumb: string
  }
}

export interface UnslashData {
  results: UnslashImage[]
  total: number
  total_pages: number
}

export interface UnslashImages {
  images: UnslashImage[]
}
