import React, { createRef, RefObject } from "react";
import { UnslashImage } from "../models/Unslash";
import "./ImageCard.scss";

class ImageCard extends React.Component<{ image: UnslashImage }, { gridSpan: number }> {

  constructor(
    props: { image: UnslashImage },
    private readonly imageRef: RefObject<HTMLImageElement>,
    private readonly gridAutoRows: number
  ) {
    super(props);
    this.imageRef = createRef();
    this.gridAutoRows = 10;
    this.state = { gridSpan: 0 }
  }

  componentDidMount(): void {
    this.imageRef.current?.addEventListener('load', () => this.setGridSpan());
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const { urls, description } = this.props.image;
    return (
      <div style={{gridRowEnd: `span ${this.state.gridSpan}`}}>
        <img ref={this.imageRef}
             className="image"
             src={urls.regular} alt={description} />
      </div>
    );
  }

  private setGridSpan = () => {
    const height = this.imageRef.current?.clientHeight;
    if (height)
    this.setState({
      gridSpan: Math.ceil(height / this.gridAutoRows ) + 1
    });
  }
}

export default ImageCard;
