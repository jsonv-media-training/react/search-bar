import React from 'react';
import axios from "axios";

import './App.scss';
import SearchBar from "./search-bar/SearchBar";
import { UnslashData, UnslashImages } from "./models/Unslash";
import { Res } from "./models/Res";
import Gallery from "./gallery/Gallery";

class App extends React.Component<any, UnslashImages> {

  state: UnslashImages = { images: [] };

  private url = 'https://api.unsplash.com/search/photos';
  private accessKey = '6U7zpsipQx9Qo02ltRQSnvwaFAdAS-U_dohria93EZY';
  private onSearch = async (key: string) => {
    const result: Res<UnslashData> = await axios.get(
      this.url,
      {
        headers: {
          Authorization: `Client-ID ${this.accessKey}`
        },
        params: {
          query: key
        }
      }
    );
    this.setState({
      images: result && result.data && result.data.results.length ? result.data.results : []
    });
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <div className="container">
        <p className="header mt-4">REACT PROJECT APP</p>
        <SearchBar invokeFnc={this.onSearch}/>
        <Gallery images={this.state.images}/>
      </div>
    );
  }
}

export default App;
